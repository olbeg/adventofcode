#include <iostream>
#include <fstream>
#include <functional>
#include <array>
#include <sstream>
#include <algorithm>

using namespace std;

class Point {
    public:
        Point(string line) {
            stringstream sx, sy;
            auto comma = line.find(',');
            sx.str(line.substr(0, comma));
            sy.str(line.substr(comma + 1, line.length()));
            sx >> x;
            sy >> y;
        }

        size_t x;
        size_t y;
};

class Coords {
    public:
        Coords(Point _1, Point _2) : _1(_1), _2(_2) {}
        Point _1; // top-left
        Point _2; // bottom-right
};

enum class Action { TURN_ON, TURN_OFF, TOGGLE };

class Grid {
    public:
        Grid() {
            lights.fill(0);
        }

        auto lit_count() -> int {
            return accumulate(begin(lights), end(lights), 0, plus<int>());
        }

        void action(Coords &&coords, Action action) {
            for (auto i = coords._1.x; i <= coords._2.x; ++i) {
                for (auto j = coords._1.y; j <= coords._2.y; ++j) {
                    auto index = i + (j * 1000);
                    if (action == Action::TURN_ON) { 
                        lights[index] += 1;
                    } else if (action == Action::TURN_OFF) {
                        if (lights[index] != 0)
                            lights[index] -= 1;
                    } else if (action == Action::TOGGLE) { 
                        lights[index] += 2;
                    }
                }
            }
        }
    
    private:
        array<int, 1000 * 1000> lights;
};

auto getCoords(string &&line) -> Coords {
    auto _1st = line.find(' ');
    auto _2nd = line.find(' ', _1st + 1);
    return { { line.substr(0, _1st) }, { line.substr(_2nd, line.length()) } };
}

int main(int argc, char *argv[]) {
    Grid grid;

    const string a = "turn on ";
    const string b = "turn off ";
    const string c = "toggle ";

    ifstream in(argv[1]);
    for (string line; getline(in, line); ) {
        if (line.find(a.c_str()) != string::npos) {
            grid.action(getCoords(line.substr(a.length(), line.length())), Action::TURN_ON);
            continue;
        }

        if (line.find(b.c_str()) != string::npos) {
            grid.action(getCoords(line.substr(b.length(), line.length())), Action::TURN_OFF);
            continue;
        }

        if (line.find(c.c_str()) != string::npos) {
            grid.action(getCoords(line.substr(c.length(), line.length())), Action::TOGGLE);
            continue;
        }
    }
    
    cout << grid.lit_count() << endl;
}







