#include <sstream>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <list>
#include <functional>
#include <map>
#include <string>
#include <memory>
#include <stdint.h>

using namespace std;
using u16 = uint16_t; 

class Wire;
using WirePtr = shared_ptr<Wire>;
map<string, WirePtr> wires;

enum class Gate { NOT, OR, AND, RSHIFT, LSHIFT, PASS };

map<string, Gate> mapping {
    { "NOT", Gate::NOT },
    { "OR", Gate::OR },
    { "AND", Gate::AND },
    { "RSHIFT", Gate::RSHIFT },
    { "LSHIFT", Gate::LSHIFT },
};


class Wire {
    public:       

        virtual u16 get() = 0;

        auto init(list<WirePtr> inputs, Gate gate) -> void {

            for (auto i : inputs) {
                if (i.get() == this) {
                    cout << _name << " has cyclic link" << endl;
                    exit(-1);
                }
            }

            _inputs = inputs;
            _gate = gate;
            is_init = true;
        }

        bool is_init = false;

        Wire(string name) : _name(name) {}
        string _name;
        list<WirePtr> _inputs;
        Gate _gate = Gate::PASS;

        string toString() {
            return _name;
        }
        
        //function<int(Gate)> gate;
        //template<class ... T> int gate(T ... args);
};

class Node : public Wire {
    public:
        Node() : Wire("") {}
        Node(string name) : Wire(name) {}
        Node(string name, list<WirePtr> inputs, Gate gate) : Wire(name) {
            init(inputs, gate);
        }

        bool done = false;
        u16 value = 0;

        auto get() -> u16 {
            cout << " -> " << _name << " ";
            if (done)
                return value;
            if (!is_init) {
                cout << _name << " not init!" << endl;
                exit(-1);
            }
            if (_gate == Gate::NOT) {
                value = ~((_inputs.front())->get());
            } else if (_gate == Gate::OR) {
                value = (((*_inputs.begin())->get()) | 
                        ((*++_inputs.begin())->get()));
            } else if (_gate == Gate::AND) {
                value = (((*_inputs.begin())->get()) &
                        ((*++_inputs.begin())->get()));
            } else if (_gate == Gate::RSHIFT) {
                value = (((*_inputs.begin())->get()) >> 
                        ((*++_inputs.begin())->get()));
            } else if (_gate == Gate::LSHIFT) {
                value = (((*_inputs.begin())->get()) <<
                        ((*++_inputs.begin())->get()));
            } else { 
                value = ((_inputs.front()->get()));
            }
            done = true;
            return value;
        }
};

class Leaf : public Wire {
    public:
        Leaf(string name) : Wire(name) {
            _value = stoi(name);
        }
        u16 _value = 0;
        auto get() -> u16 {
            cout << " -> " << _name << endl;
            if (is_init) {
                cout << _name << " is init!" << endl;
                exit(-1);
            }
            return _value;
        }
};

bool isNumber(string value) {
    for (auto v : value)
        if (!isdigit(v))
            return false;
    return true;
}

void initWire(string name) {
    if (wires.find(name) == wires.end()) {
        if (isNumber(name))
            wires[name] = make_shared<Leaf>(name);
        else
            wires[name] = make_shared<Node>(name);
    }
}

void process(string value, string out) {
    Gate gate = Gate::PASS;
    for (auto m : mapping) {
        if (value.find(m.first) != string::npos)
            gate = m.second;
    }
    if (gate == Gate::PASS && isNumber(value)) {
        if (wires.find(value) == wires.end())
            wires[value] = make_shared<Leaf>(value);
        wires[out]->init({ wires[value] }, gate);
    }

    if (gate == Gate::NOT) {
        auto pos = value.find(' ');
        auto name = value.substr(pos + 1, value.length());
        if (wires.find(name) == wires.end())
            wires[name] = make_shared<Node>(name);
        wires[out]->init({ wires[name] }, gate);
    } else {
        auto pos = value.find(' ');
        auto name0 = value.substr(0, pos);
        auto rpos = value.rfind(' ');
        auto name1 = value.substr(rpos + 1, value.length());
        
        initWire(name0);
        initWire(name1);
        wires[out]->init({ wires[name0], wires[name1] }, gate);
    }
}

int main(int argc, char *argv[]) {
    
    string arrow = " -> ";
    ifstream in(argv[1]);
    for (string line; getline(in, line); ) {
        auto p = line.find(arrow);
        string out = line.substr(p + arrow.length(), line.length());

        if (wires.find(out) == wires.end())
            wires[out] = make_shared<Node>(out); 

        string tmp = line.substr(0, p);
        process(tmp, out);
    }

    for (auto w : wires) {
        cout << w.second->get() << endl;
    }
    
    cout << "######" << endl;
    cout << wires["a"]->get() << endl;
    cout << wires["b"]->get() << endl;

    return 0;
}
