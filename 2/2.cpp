#include <iostream>
#include <iterator>
#include <string>
#include <sstream>
#include <fstream>
#include <array>
#include <algorithm>
#include <functional>

using namespace std;

constexpr unsigned int N = 3;

auto split(string input) -> array<int, N> {
    array<int, N> tmp;
    unsigned int pos = 0;
    auto trim = [&pos] (string input) {
        stringstream stream;
        int result;
        auto next_pos = input.find("x", pos);
        if (pos == string::npos) {
            stream << input.substr(pos, input.length() - pos);
        } else {
            stream << input.substr(pos, next_pos - pos);
            pos = ++next_pos; 
        }
        stream >> result;
        return result;
    };
    for (size_t i = 0; i < N; ++i)
        tmp[i] = trim(input);
    sort(begin(tmp), end(tmp));
    return tmp;
}

int main(int argc, char *argv[]) {
    ifstream foo(argv[1]);
    auto result = 0;
    for (string line; getline(foo, line) ;) {
        auto tmp = split(line);
        result += tmp[0] * 2 + tmp[1] * 2 + accumulate(begin(tmp), end(tmp), 1, multiplies<int>());
    }
    cout << result << endl;
    return 0;
}
