#include <iostream>
#include <openssl/md5.h>

using namespace std;

int main(int argc, char *argv[]) {
    const unsigned char data[] = { 'a', 'b', 'c', 'd', 'e', 'f', '6', '0', '9', '0', '4', '3' };
    /*
    const void * p_data = (void*)data;
    MD5_CTX *c;
    MD5_Init(c);
    MD5_Update(c, p_data, 16);
    unsigned char *md;
    MD5_Final(md, c);
    printf("%02X\n", md);
    */
    unsigned char md[16];
    unsigned char *ptr = MD5(data, 16, md);
    for (int i = 0; i < 10; ++i) {
        cout << (int)md[i] << " ";
    }
    cout << endl;
    return 0;
}
