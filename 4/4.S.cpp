#include <cmath>
#include <iostream>
#include <string.h>
#include <sstream>
#include <openssl/md5.h>
 
using namespace std;

int main() {

    //std::string key = "abcdef609043"; 
    auto calc = [] (std::string key, int64_t number) {
        //cout << key << endl;
        unsigned char digest[MD5_DIGEST_LENGTH];
        char *c_string = (char*)malloc(255);
        strcpy(c_string, key.c_str());
        MD5((unsigned char*)c_string, key.size(), (unsigned char*)&digest);    
        char mdString[33];
        if (digest[0] == 0 && digest[1] == 0 && digest[2] == 0) {
            cout << "AZAZA " << number << endl;
            exit(0);
        } 
        free(c_string);
    };

    int64_t number = 0;
    std::string base_key = "ckczppom";
    stringstream s(base_key);
    std::string tmp;
    while(true) {
        s << base_key << number;
        tmp = s.str();
        calc(tmp, number++); 
        s.str("");
        s.clear();
    }
    return 0;
}
