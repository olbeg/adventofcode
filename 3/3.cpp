#include <iostream>
#include <fstream>
#include <iterator>
#include <list>
#include <set>
#include <array>

using namespace std;

set<array<int, 2>> data;
#define X 0
#define Y 1

int main(int argc, char *argv[]) {
    ifstream input(argv[1]);
    array<int, 2> santa = {0, 0};
    array<int, 2> robot = {0, 0};

    auto move_smbdy = [] (char byte, array<int, 2> *current) {
        switch(byte) {
            case '^' : ++((*current)[Y]); break;
            case '>' : ++((*current)[X]); break;
            case '<' : --((*current)[X]); break;
            case 'v' : --((*current)[Y]); break;
        }
    };

    array<int, 2> *current;
    for (size_t i = 0; !input.eof(); ++i) {
        if (i % 2 == 0)
            current = &santa;
        else
            current = &robot;
        char byte;
        input.get(byte);
        move_smbdy(byte, current);
        data.insert(*current);
    }
    cout << data.size() << " " << (*current)[X] << ":" << (*current)[Y] << endl;
    return 0;
}
