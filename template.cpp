#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char *argv[]) {
    
    ifstream in(argv[1]);
    for (string line; getline(in, line); ) {
        cout << line << endl;
    }

    return 0;
}
