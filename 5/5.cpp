#include <cstdio>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <set>
#include <array>

using namespace std;

bool count_pairs(string &line) {
    set<array<int, 2>> words;
    set<array<int, 3>> data;
    int index = 0;
    for (size_t i = 0; i < line.length() - 1; ++i)  {
        if (data.find({line[i], line[i + 1], index - 1}) == data.end())
            data.insert({line[i], line[i + 1], index++});
        words.insert({line[i], line[i + 1]});
    }

    auto cnt = count_if(begin(words), end(words), [&data, &line](auto value) {
        auto local_cnt = 0;
        for (auto d : data) {
            if (local_cnt > 1)
                return true;
            if (value[0] == d[0] && value[1] == d[1]) {
                local_cnt++;
                if (local_cnt > 1)
                    printf("%s : %c%c\n", line.c_str(), d[0], d[1]);
            }
        }
        return (local_cnt > 1);
            
    });
    return (cnt > 0);
}

bool count_triples(string &line) {
    for (size_t i = 0; i < line.length() - 2; ++i)
        if (line[i] == line[i + 2]) {
            cout << line << " : " << line[i] << line[i + 1] << line[i + 2] << endl; 
            return true;
        }
    return false;
}

int main(int argc, char *argv[]) {
    ifstream in(argv[1]);
    auto cnt = 0;
    for (string line; getline(in, line); ) {
        cout << " v " << line << " v " <<endl;
        if (count_pairs(line) && count_triples(line)) {
            cout << " ^ true ^ " << ++cnt << endl;
        }
    }
    cout << cnt << endl;
    return 0;
}
