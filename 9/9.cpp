#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <algorithm>
#include <memory>
#include <stdint.h>

using namespace std;

class Vertex;
using VertexPtr = shared_ptr<Vertex>;

map<string, VertexPtr> vertices;

class Vertex {
    public:

        Vertex(string name) : mName(name) {}
        void init(map<string, uint> links) {
            mLinks = links;
        }
        void addLink(string name, uint weight) {
            mLinks.insert(make_pair(name, weight));
            vertices.find(name)->second->addBackLink(mName, weight);
        }
        void addBackLink(string name, uint weight) {
            mLinks.insert(make_pair(name, weight));
        }

        string mName;
        map<string, uint> mLinks;
        set<string> mVisited;
        set<string> mPrevPath;

        auto goround(set<string> prevPath, uint weight) -> uint {
            auto result = weight;
            mVisited.clear();
            for (auto p : prevPath) {
                cout << p << " ";
                if (mLinks.find(p) != mLinks.end())
                    mVisited.insert(p);
            }
            cout << mName << " -> ";
            prevPath.insert(mName);
            
            for_each(begin(mLinks), end(mLinks), [&](auto link) {
                string name = link.first;
                if (mVisited.find(name) == mVisited.end()) {
                    result += vertices.find(name)->second->goround(prevPath, link.second);
                    if (prevPath.size() == 2) {
                        cout << result << endl;
                        result = weight;
                    }
                    mVisited.insert(name);
                }
            });
            if (prevPath.size() == vertices.size())
                cout << endl;
            return result;
        }
};

class Tour {
    public:
        Tour(string name) : mStart(vertices.find(name)->second) {
            set<string> empty;
            mStart->goround(empty, 0);
        }

        VertexPtr mStart;
};

int main(int argc, char *argv[]) {
    
    ifstream in(argv[1]);
    for (string line; getline(in, line); ) {
        cout << line << endl;
    }
    
    VertexPtr a = make_shared<Vertex>("a");
    VertexPtr b = make_shared<Vertex>("b");
    VertexPtr c = make_shared<Vertex>("c");
    VertexPtr d = make_shared<Vertex>("d");
    vertices.insert(make_pair(a->mName, a));
    vertices.insert(make_pair(b->mName, b));
    vertices.insert(make_pair(c->mName, c));
    vertices.insert(make_pair(d->mName, d));
    
    a->addLink(b->mName, 1);
    a->addLink(c->mName, 2);
    c->addLink(d->mName, 2);
    b->addLink(d->mName, 1);
    b->addLink(c->mName, 1);

    Tour("a");

    return 0;
}
