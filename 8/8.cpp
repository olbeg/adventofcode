#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;

string process(string line, string pattern, int count, auto *literals) {
    auto pos = line.find(pattern);
    while (pos != string::npos) {
        auto head = line.substr(0, pos);
        auto tail = line.substr(pos + count, line.length());
        line = head.append(tail);
        pos = line.find(pattern);
        *literals -= (count - 1);
    }
    return line;
}

int main(int argc, char *argv[]) {
    ifstream in(argv[1]);
    auto result = 0;
    for (string line; getline(in, line); ) {
        cout << line << endl;

        auto total_new = accumulate(begin(line), end(line), 2, [](auto acc, auto byte) {
            if (byte == '\"' || byte == '\\')
                ++acc;
            return ++acc;
        });
        auto total = line.length();
        auto literals = total - 2;

        line = process(line, "\\\\", 2, &literals);
        line = process(line, "\\x", 4, &literals);
        line = process(line, "\\\"", 2, &literals);

        if (line.find("\\") != string::npos) {
            cout << line << " ERROR" << endl;
            exit(-1);
        }

        //cout << line << " " << total << " " << literals << endl;
        //result += (total - literals);
        result += (total_new - total);
    }
    cout << result << endl;
    return 0;
}
